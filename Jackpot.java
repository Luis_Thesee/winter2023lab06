public class Jackpot{
	public static void main (String[] args){
		Board board = new Board();
		boolean gameOver = false	;
		int numOfTilesClosed = 0;
		System.out.println("Board:");
		while (!gameOver){
			System.out.println(board);
			System.out.println("\n");
			if (board.playATurn()){
				gameOver = true;
			}
			else {
				numOfTilesClosed += 1;
			}
		}

		
		if ( numOfTilesClosed >=7){
			System.out.println("You just reached a Jackpot, YOU WIN!");
		}
		else{
			System.out.println("You lost :(");
		}
	}
}